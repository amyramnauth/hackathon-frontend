
// Pages
import Login from 'src/components/Dashboard/Views/Pages/Login.vue'
import Register from 'src/components/Dashboard/Views/Pages/Register.vue'
import RegisterProperty from 'src/components/Dashboard/Views/Pages/RegisterProperty.vue'
import SecretQuote from 'src/components/Dashboard/Views/Pages/SecretQuote.vue'
import ExtendedTables from 'src/components/Dashboard/Views/Tables/ExtendedTables.vue'
import PaginatedTables from 'src/components/Dashboard/Views/Tables/PaginatedTables.vue'
import UserProperties from 'src/components/Dashboard/Views/Tables/UserProperties.vue'
import Payment from 'src/components/Dashboard/Views/Tables/Payment.vue'
import UserPayment from 'src/components/Dashboard/Views/Tables/UserPayment.vue'
// import Payment from 'src/components/Dashboard/Views/Tables/Payment.vue'
import Wizard from 'src/components/Dashboard/Views/Forms/Wizard.vue'
import RegularForms from 'src/components/Dashboard/Views/Forms/RegularForms.vue'
import Report from 'src/components/Dashboard/Views/Pages/Report.vue'
import Application from 'src/components/Dashboard/Views/Pages/Application.vue'
import UserNotifications from 'src/components/Dashboard/Views/Pages/UserNotifications.vue'
import Discounts from 'src/components/Dashboard/Views/Pages/Discounts.vue'
import NotFound from '../components/GeneralViews/NotFoundPage.vue'
import MakePayment from 'src/components/Dashboard/Views/Forms/MakePayment.vue'
import GoogleMaps from 'src/components/Dashboard/Views/Maps/GoogleMaps.vue'
import Sample from 'src/components/Dashboard/Views/Pages/Sample.vue'
import IncidentReportForms from 'src/components/Dashboard/Views/Forms/IncidentReportForms.vue'
import Public from 'src/components/Dashboard/Views/Tables/PublicData.vue'
import UpdateProp from 'src/components/Dashboard/Views/Pages/EditProperty.vue'


let loginPage = {
  path: '/login',
  name: 'Login',
  component: Login,
}

let registerPage = {
  path: '/register',
  name: 'Register',
  component: Register,
}

let secretquotePage = {
  path: '/secretquote',
  name: 'SecretQuote',
  component: SecretQuote,
}

let extendedTablesPage = {
  path: '/tables',
  name: 'ExtendedTables',
  component: ExtendedTables
}

let userNotification = {
  path: '/userNotifications',
  name: 'UserNotifications',
  component: UserNotifications
}

let incidentReport = {
  path: '/incident',
  name: 'IncidentReportForms',
  component: IncidentReportForms
}

let discounts = {
  path: '/discounts',
  name: 'Discounts',
  component: Discounts
}

let paginatedTablesPage = {
  path: '/paginated',
  name: 'PaginatedTables',
  component: PaginatedTables
}

let addProperty = {
  path: '/addProperty',
  name: 'RegisterProperty',
  component: RegisterProperty
}

let paymentsPage = {
  path: '/payments',
  name: 'Payment',
  component: Payment
}

let wizard = {
  path: '/wizard',
  name: 'Wizard',
  component: Wizard
}

let RegularFormsPage = {
  path: '/notifications',
  name: 'RegularForms',
  component: RegularForms
}

let report = {
  path: '/reports',
  name: 'Report',
  component: Report
}

let application = {
  path: '/applications',
  name: 'Application',
  component: Application
}

let createPayment = {
  path: '/create-payment',
  name: 'MakePayment',
  component: MakePayment
}

let googlePayments = {
  path: '/maps',
  name: 'GooglePayment',
  component: GoogleMaps
}

let samp = {
  path: '/sample',
  name: 'Sample',
  component: Sample
}

let PubData = {
  path: '/public-data',
  name: 'PublicData',
  component: Public
}

let MyProperties = {
  path: '/my-properties',
  name: 'UserProperties',
  component: UserProperties
}

let UserPayments = {
  path: '/my-payments',
  name: 'UserPayments',
  component: UserPayment
}

let EditProperty = {
    path: '/editprop',
    name: 'UpdateProp',
    component: UpdateProp
}

const routes = [
  {
    path: '/',
    component: Login,
    redirect: '/login',
  },
  loginPage,
  registerPage,
  secretquotePage,
  extendedTablesPage,
  paginatedTablesPage,
  wizard,
  paymentsPage,
  RegularFormsPage,
  report,
  application,
  addProperty,
  userNotification,
  discounts,
  createPayment,
  googlePayments,
  incidentReport,
  samp,
  PubData,
  EditProperty,
  MyProperties,
  UserPayments,

  {path: '*', component: NotFound}
]

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
 function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/


export default routes
// export default new VueRouter({
//   routes: [
//     {
//       path: '/',
//       component: Login,
//       redirect: '/login',
//     },
//     {
//       path: '/secretquote',
//       component: SecretQuote,
//     },
//   ]
// })
