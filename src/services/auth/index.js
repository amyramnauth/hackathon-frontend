import { router } from '../../main'
import constants from '../constants'

export default {

  // User object will let us check authentication status
  user: {
    authenticated: false,
    isStaff: false
  },

  // Send a request to the login URL and save the returned JWT
  login(context, creds, redirect) {
    context.$http.post(constants.API_URL+constants.API_VERSION+'/login', creds).then(response => {
        //Success
        console.log(response.data)
        console.log('access token', response.data.access_token);
        localStorage.setItem('access_token', response.data.access_token)

        this.user.authenticated = true
        if (response.data.position_id != null) {
          this.user.isStaff = true
          // console.log('user staff is', this.user.isStaff)
        }
        // console.log(this.user.isStaff)
          // Redirect to a specified route
          if(redirect) {
            router.push(redirect)         
          }
      }, response => {
          // error callback
          console.log(response.data)
        });
  },

  signup(context, creds, redirect) {
    context.$http.post(constants.API_URL+constants.API_URL+'/register', creds).then(register => {
      //Success
      localStorage.setItem('access_token', register.data.access_token)

      this.user.authenticated = true
        // Redirect to a specified route
        if(redirect) {
          router.push(redirect)         
        }
    }, register => {
        // error callback
        console.log(register.data)
      });
  },

  // To log out, we just need to remove the token
  logout(redirect) {
    // localStorage.removeItem('id_token')
    localStorage.removeItem('access_token')
    this.user.authenticated = false
    router.push(redirect)
    // console.log(this.user.authenticated)
  },

  checkAuth() {
    var jwt = localStorage.getItem('access_token')
    if(jwt) {
      this.user.authenticated = true
    }
    else {
      this.user.authenticated = false      
    }
    var isStaff = this.user.isStaff
    if (isStaff == true ) {
      this.user.isStaff = true
    }
    else {
      this.user.isStaff = false      
    }
    this.getAuthHeader();
  },

  // The object to be passed as a header for authenticated requests
  getAuthHeader() {
    return {
      // 'Access-Control-Allow-Origin': *, 
      headers: {'Authorization': 'Bearer ' + localStorage.getItem('access_token')}
    }
  }
}